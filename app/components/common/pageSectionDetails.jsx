import React, { Component } from 'react'
import { arrayOf, any, string } from 'prop-types'
import { v1 as uuid } from 'uuid'
import { copyToClipboard } from '../utilities'

class PageSectionDetails extends Component {
  static propTypes = {
    sectionHeading: string,
    content: arrayOf(any)
  }

  static defaultProps = {
    sectionHeading: 'Heading',
    content: []
  }

  constructor(props) {
    super(props)

    this.state = {
      sectionHeading: props.sectionHeading,
      content: props.content
    }
  }

  render() {
    const {
      sectionHeading, content
    } = this.state

    let copyAbleID

    return (
      <div className="mb_pageSection col-sm-6 text-break">
        {sectionHeading && <h3>{sectionHeading}</h3>}

        {content.length > 0 && content.map((detail) => {
          copyAbleID = uuid()
          return (
            <div key={copyAbleID} className="mb_details">
              <div className="col-3 p-0">{detail.title}:</div>
              <div className="col-sm p-0" id={detail.canCopy && detail.canCopy === 'true' && `detailValue-${copyAbleID}`}>
                {detail.value}
                {detail.canCopy && detail.canCopy === 'true'
                && (
                <button className="btn btn-default p-0 ml-3" type="button" element={`detailValue-${copyAbleID}`} onClick={copyToClipboard}>
                  <i className="fa fa-copy" />
                </button>
                )}
              </div>
            </div>
          )
        })}
      </div>
    )
  }
}

export default PageSectionDetails
