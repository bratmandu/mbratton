import React, { Component } from 'react'
import { arrayOf, any, string } from 'prop-types'
import { v1 as uuid } from 'uuid'

class PageSection extends Component {
  static propTypes = {
    sectionHeading: string,
    content: arrayOf(any)
  }

  static defaultProps = {
    sectionHeading: 'Heading',
    content: []
  }

  constructor(props) {
    super(props)

    this.state = {
      sectionHeading: props.sectionHeading,
      content: props.content
    }
  }

  render() {
    const {
      sectionHeading, content
    } = this.state

    return (
      <div className="mb_pageSection col-12">
        {sectionHeading && <h3>{sectionHeading}</h3>}

        {content.length > 0 && content.map(education => (
          <ul key={uuid()} className="mb_education container">
            <li className="row strong">{education.years}</li>
            <li className="row font-italic">{education.location}</li>
            <li className="row font-italic">Level: {education.level}</li>
            <li className="row">
              {education.summary.length > 3 && (
                education.summary.map((summary, index) => (
                  <div key={uuid()} className={`px-0 col-md-6 ${index < education.summary.length - 1 && 'border-bottom border-secondary'}`}>
                    <div className="px-0 col-md-6 float-md-left mb_education-info">{summary.course}: </div>
                    <div className="px-0 col-md-6 float-md-right strong mb_education-info">{summary.grade}</div>
                  </div>
                ))
              )}
              {education.summary.length < 4 && (
                education.summary.map((summary, index) => (
                  <div key={uuid()} className="mb_education-summary">{summary.course} <strong>{summary.grade}</strong>{education.summary.length > 1 && index < education.summary.length - 1 && ','}</div>
                ))
              )}
            </li>
          </ul>
        ))}
      </div>
    )
  }
}

export default PageSection
