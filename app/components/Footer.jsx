import React, { Component } from 'react'
import ScrollTop from './utilities/scrollTop'
import footerContent from './content/footer.json'

class Footer extends Component {
  state = {
    footerText: 'footer',
    topButtonText: 'top',
    linkedinUrl: 'www.google.com',
    linkedinText: 'google',
    repoUrl: 'www.google.com',
    repoText: 'repo'
  }

  componentDidMount() {
    this.setState({
      footerText: footerContent.footerText,
      topButtonText: footerContent.topButtonText,
      linkedinUrl: footerContent.linkedinUrl,
      linkedinText: footerContent.linkedinText,
      repoUrl: footerContent.repoUrl,
      repoText: footerContent.repoText
    })
  }

  render() {
    const {
      footerText, topButtonText, linkedinUrl, linkedinText, repoUrl, repoText
    } = this.state

    return (
      <footer className="px-3 py-2 fixed-bottom">
        <div className="container">
          <div className="row">
            <div className="col-sm align-middle pl-0">
              {footerText}
            </div>
            <div className="float-right">
              <a
                target="_blank"
                rel="noopener noreferrer"
                href={linkedinUrl}
                className="mr-4 align-middle"
              >
                {linkedinText}
              </a>
              <a
                target="_blank"
                rel="noopener noreferrer"
                href={repoUrl}
                className="mr-4 align-middle"
              >
                {repoText}
              </a>
              <button type="button" className="btn btn-primary" onClick={ScrollTop}>
                {topButtonText}
              </button>
            </div>
          </div>
        </div>
      </footer>
    )
  }
}

export default Footer
