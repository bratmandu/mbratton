import React, { Component } from 'react'
import { v1 as uuid } from 'uuid'
import pageContent from './content/frontPage.json'
import IntroText from '../common/introText'
import PageSectionDefault from '../common/pageSectionDefault'

class FrontPageComponent extends Component {
  state = {
    introText: 'Welcome',
    pageSections: []
  }

  componentDidMount() {
    this.setState({
      introText: pageContent.introText,
      pageSections: pageContent.pageSections
    })
  }

  render() {
    const { introText, pageSections } = this.state

    return (
      <div className="mb_page">
        <IntroText introText={introText} />
        { pageSections.length && pageSections.map(pageSection => (
          <PageSectionDefault
            key={uuid()}
            sectionHeading={pageSection.sectionHeading}
            content={pageSection.textParagraphs}
          />
        ))
        }
      </div>
    )
  }
}

export default FrontPageComponent
