import React, { Component } from 'react'
import { v1 as uuid } from 'uuid'
import Modal from './components/modal'
import AddOrderForm from './components/addOrderForm'
import OrderDetail from './components/orderDetail'
import Alert from './components/alert'
import Search from './components/search'
import Button from './components/button'
import Input from './components/input'
import Table from './components/table'

class SampleApp extends Component {
  state = {
    data: [],
    alertActive: false,
    sortable: {
      heading: '',
      order: 'asc'
    },
    filterTerm: '',
    price: 0,
    quantity: 0,
    modal: null
  }

  getData = (searchTerm) => {
    fetch(`https://www.${searchTerm}.io/v2/5b7a786f3400003f008ed487`)
      .then(response => this.handleFetchResponse(response))
      .then(data => this.handleSuccessResponse(data))
      .catch(() => this.handleErrorResponse())
  }

  setAlert = (props) => {
    const { alertActive } = this.state

    this.setState({
      alertActive: !alertActive,
      ...props
    })
  }

  toggleAlert = (props) => {
    const { alertActive } = this.state

    if (alertActive) {
      this.resetAlert()
        .then(() => this.setAlert(props))
    } else {
      this.setAlert(props)
    }
  }

  resetAlert = () => {
    const promise = new Promise((resolve) => {
      this.setState({
        alertActive: false
      }, () => resolve())
    })

    return promise
  }

  sortTable = (heading) => {
    const { sortable } = this.state

    this.setState({
      data: this.sortData(this.originalData, heading, sortable.order),
      sortable: {
        heading,
        order: sortable.order === 'asc' ? 'desc' : 'asc'
      }
    })
  }

  sortData = (array = [], key, sortDirection = 'asc') => array.slice().sort((a, b) => (
    sortDirection === 'asc'
      ? parseFloat(a[key]) - parseFloat(b[key])
      : parseFloat(b[key]) - parseFloat(a[key])
  ))

  handleInputChange = ({ target: { value } }) => {
    const filtered = this.originalData.filter(row => this.exists(row, 'account', value))

    this.setState({
      filterTerm: value,
      data: filtered
    }, () => this.totalFiltered())
  }

  exists = (obj, key, term) => obj[key].toLowerCase().indexOf(term.toLowerCase()) >= 0

  calcTotal = (array, key) => (
    array.length !== 0 ? array.reduce((acc, obj) => acc + obj[key], 0) : 0
  )

  updateTotal = (dir, key, id) => {
    const { data } = this.state

    this.setState(prevState => ({
      [key]: dir === 'up' ? prevState[key] + 1 : prevState[key] - 1
    }))

    const modelToUpdate = data.filter(record => record.id.toString() === id)
    if (modelToUpdate[0]) {
      modelToUpdate[0][key] = dir === 'up' ? modelToUpdate[0][key] + 1 : modelToUpdate[0][key] - 1
    }
  }

  totalFiltered = () => {
    const { data } = this.state
    this.setState({
      price: this.calcTotal(data, 'price'),
      quantity: this.calcTotal(data, 'quantity')
    })
  }

  addOrder = (details) => {
    const { data } = this.state
    data.push(details)
    this.setState({
      data
    }, () => this.totalFiltered())
    this.originalData = data
    this.hideModal()
  }

  addOrderModal = () => {
    const modal = this.createModal('Add Order')
    this.setState({
      modal
    })
  }

  showModal = (props, id, account) => {
    const modal = this.createModal(props, id, account)
    this.setState({
      modal
    })
  }

  hideModal = () => {
    this.setState({
      modal: null
    })
  }

  handleFetchResponse = (response) => {
    if (response.ok) {
      return response.json()
    }
    throw new Error('Error with fetch!')
  }

  handleErrorResponse = () => {
    this.toggleAlert({
      alertType: 'danger',
      alertMessage: 'Error with response!'
    })
  }

  handleSuccessResponse = (data) => {
    this.toggleAlert({
      alertType: 'success',
      alertMessage: 'Successful response!'
    })

    this.originalData = data

    this.setState({
      data,
      price: this.calcTotal(data, 'price'),
      quantity: this.calcTotal(data, 'quantity')
    })
  }

  deleteRow = (id) => {
    const { data } = this.state
    const filteredData = data.filter(record => record.id.toString() !== id)

    this.setState({
      data: filteredData
    }, () => this.totalFiltered())

    this.hideModal()
  }

  createModal(props, id, account) {
    return (
      <Modal
        title={typeof props === 'string' ? props : 'Additional Info'}
        size="small"
        handleCloseClick={this.hideModal}
      >
        {props === 'Add Order' && (
          <AddOrderForm
            addOrder={this.addOrder}
            id={uuid()}
          />
        )}

        {typeof props !== 'string' && <OrderDetail {...props} />}

        {props === 'Delete Record?' && (
          <div className="text-center">
            <div className="">{account}</div>
            <button className="btn btn-success m-3" type="button" onClick={() => this.deleteRow(id)}>Confirm</button>
          </div>
        )}
      </Modal>
    )
  }

  render() {
    const {
      data,
      alertActive,
      alertType,
      alertMessage,
      sortable,
      filterTerm,
      price,
      quantity,
      modal
    } = this.state

    const isDisabled = data.length === 0

    return (
      <div>
        { alertActive && <Alert type={alertType}>{alertMessage}</Alert> }
        { modal }
        <div>
          <p>This sample application demonstrates some of what I can do with React.</p>
          <p>Here, I can use the search term to complete a url, from which I fetch json data to populate the table, which can be sorted, and shows current totals. Some orders are designated as filled and therefore cannot be edited.</p>
          <p>I can then add new records, delete or view further info about an account in a modal. Price/quantity can be ticked up or down, and the totals will update accordingly.</p>
        </div>
        <Search onSearch={searchTerm => this.getData(searchTerm)} />
        <div className="d-flex flex-row mb-3 flex-wrap">
          { data.length > 0 && (
            <Button
              className="btn btn-primary mr-auto text-nowrap mb-3"
              onClick={() => this.addOrderModal(data)}
            >
              <i className="fa fa-plus button-icon pr-2" />
              Add Order
            </Button>
          )}
          { !data.length && (
            <Button
              className="btn btn-primary mr-auto text-nowrap mb-3"
              disabled
            >
              <i className="fa fa-plus button-icon pr-2" />
              Add Order
            </Button>
          )}
          <Input
            id="filter"
            name="filter"
            type="text"
            placeholder="Filter accounts..."
            className=""
            value={filterTerm}
            onChange={this.handleInputChange}
            disabled={isDisabled}
          />
        </div>
        <Table
          data={data}
          handleSort={this.sortTable}
          sorting={sortable}
          totalQuantity={quantity}
          totalPrice={price}
          updateTotal={this.updateTotal}
          handleShowModal={this.showModal}
        />
      </div>
    )
  }
}

export default SampleApp
