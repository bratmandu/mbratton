import React from 'react'

function Oops() {
  return (
    <h1>Oops, something went wrong.</h1>
  )
}

export default Oops
