import React, { Component } from 'react'
import {
  number, bool, string, func, node
} from 'prop-types'
import Button from './button'
import { incrementValue, decrementValue } from '../../utilities'

const propTypes = {
  account: string,
  contract: string,
  price: number,
  quantity: number,
  filled: bool,
  updateTotal: func,
  handleShowModal: func,
  id: string
}

const defaultProps = {
  account: 'N/A',
  contract: 'N/A',
  price: 0,
  quantity: 0,
  filled: false,
  updateTotal: () => {},
  handleShowModal: () => {},
  id: '0'
}

const tickTableCellPropTypes = {
  filledStatus: bool,
  children: node.isRequired,
  handleClick: func,
  type: string.isRequired
}

const tickTableCellDefaultProps = {
  filledStatus: false,
  handleClick: () => {}
}

function TickTableCell({
  type,
  filledStatus,
  children,
  handleClick
}) {
  return (
    <td className="text-center">
      {!filledStatus && (
        <Button
          className="btn btn-link tick-down"
          onClick={() => handleClick('down', type)}
          disabled={children === 0 && true}
        >
          <i className="fa fa-chevron-circle-down" />
        </Button>
      )}
      {children}
      {!filledStatus && (
        <Button
          className="btn btn-link tick-up"
          onClick={() => handleClick('up', type)}
        >
          <i className="fa fa-chevron-circle-up" />
        </Button>
      )}
    </td>
  )
}

class TableRow extends Component {
  constructor(...args) {
    super(...args)

    const { price, quantity, filled } = this.props
    this.state = { price, quantity, filled }
  }

  onTick = (dir, type) => {
    const { updateTotal, id } = this.props

    this.setState((prevState, props) => {
      const current = prevState[type]
      const updatedValue = dir === 'up' ? incrementValue(current) : decrementValue(current)
      const filled = updatedValue - props[type] >= 10

      return {
        [type]: updatedValue,
        filled
      }
    })

    updateTotal(dir, type, id)
  }

  render() {
    const {
      account,
      contract,
      handleShowModal,
      id
    } = this.props
    const { price, quantity, filled } = this.state

    return (
      <tr className={`id=${id} ${filled ? 'is-filled' : null}`}>
        <td className="cursor-pointer cell-hover">
          { !filled && (
            <button className="btn btn-link" type="button" onClick={() => handleShowModal(this.props)}>
              {account}
            </button>
          )}
          { filled && (
            <div>{ account }</div>
          )}
        </td>
        <td className="text-center">{contract}</td>
        <TickTableCell
          type="price"
          filledStatus={filled}
          handleClick={this.onTick}
        >
          {price}
        </TickTableCell>
        <TickTableCell
          type="quantity"
          filledStatus={filled}
          handleClick={this.onTick}
        >
          {quantity}
        </TickTableCell>
        <td className="text-center">
          { !filled && (
            <Button
              className="btn btn-danger"
              onClick={() => handleShowModal('Delete Record?', id, account)}
            >
              <i className="fa fa-close button-icon pr-2" />
              Delete
            </Button>
          )}
        </td>
      </tr>
    )
  }
}

TableRow.propTypes = propTypes
TableRow.defaultProps = defaultProps
TickTableCell.propTypes = tickTableCellPropTypes
TickTableCell.defaultProps = tickTableCellDefaultProps

export default TableRow
