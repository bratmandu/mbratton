import React, { Component } from 'react'
import { string, node, func } from 'prop-types'

export default class Modal extends Component {
  static propTypes = {
    size: string,
    children: node,
    handleCloseClick: func.isRequired,
    title: string,
    classes: string
  }

  static defaultProps = {
    size: 'small',
    title: '',
    children: '',
    classes: ''
  }

  componentDidMount() {
    document.addEventListener('keydown', this.handleKeyDown, false)
  }

  componentWillUnmount() {
    document.removeEventListener('keydown', this.handleKeyDown, false)
  }

  backdropClick = ({ target: { classList } }) => {
    const { handleCloseClick } = this.props
    if (classList[0] === 'modal-backdrop') {
      handleCloseClick()
    }
  }

  handleKeyDown = ({ keyCode }) => {
    const { handleCloseClick } = this.props
    if (keyCode === 27) {
      handleCloseClick()
    }
  }

  render() {
    const {
      title,
      size,
      children,
      handleCloseClick,
      classes,
      ...rest
    } = this.props
    let modalClasses = `modal modal-dialog modal-open modal-size-${size}`
    if (classes) {
      modalClasses = modalClasses.concat(classes)
    }

    console.log('show modal: ', this.props)

    return (
      <div
        className="modal-backdrop modal-backdrop-open"
        onClick={this.backdropClick}
        role="presentation"
      >
        <div className={modalClasses} {...rest} role="dialog">
          <div className="modal-inner">
            <header className="d-flex flex-row modal-header mb-2" aria-labelledby="modal__title">
              {title.trim().length && <h2 className="modal-title">{title}</h2>}
              <button className="btn btn-link m-0 p-0" type="button" aria-label="Close" onClick={handleCloseClick}>
                <i className="fa fa-times icon-modal-close" />
              </button>
            </header>
            <div
              className="p-3"
              role="dialog"
              aria-hidden="false"
              aria-labelledby="modal__title"
              aria-describedby="modal__description"
            >
              {children}
            </div>
          </div>
        </div>
      </div>
    )
  }
}
