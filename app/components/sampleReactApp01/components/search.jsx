import React, { Component } from 'react'
import { func } from 'prop-types'
import Button from './button'

class Search extends Component {
  static propTypes = {
    onSearch: func.isRequired
  }

  state = {
    searchTerm: ''
  }

  handleSubmit = () => {
    const { searchTerm } = this.state
    const { onSearch } = this.props
    onSearch(searchTerm)
  }

  handleChange = ({ target: { value } }) => {
    this.setState({
      searchTerm: value
    })
  }

  render() {
    const { searchTerm } = this.state
    const btnDisabled = (searchTerm.length === 0) === true

    return (
      <div className="search-box">
        <div className="pt-2 px-4">
          (use &apos;mocky&apos; to return results generated from mocky.io, any other search term will return error)
        </div>
        <ul className="d-flex flex-row p-4 flex-wrap">
          <li className="m-0 row align-items-center mb-3">
            <label htmlFor="search" className="mr-2 my-0">
              <input
                id="search"
                name="search"
                type="text"
                placeholder="Enter a search term..."
                className="mr-1"
                value={searchTerm}
                onChange={this.handleChange}
              />
            </label>
          </li>
          <li className="">
            <Button className="btn btn-primary mr-2" disabled={btnDisabled} onClick={this.handleSubmit}>
              Search
            </Button>
          </li>
        </ul>
      </div>
    )
  }
}

export default Search
