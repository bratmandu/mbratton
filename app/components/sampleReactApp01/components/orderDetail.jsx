import React from 'react'
import { string } from 'prop-types'

const propTypes = {
  avatar: string,
  account: string,
  location: string,
  country: string,
  currency: string,
  sector: string
}

const defaultProps = {
  avatar: 'https://robohash.org/officiisametconsequuntur.jpg?size=50x50&set=set1',
  account: 'N/A',
  location: 'N/A',
  country: 'N/A',
  currency: 'N/A',
  sector: 'N/A'
}

function OrderDetail({
  avatar,
  account,
  location,
  country,
  currency,
  sector
}) {
  return (
    <div>
      <div className="">
        <div className="">
          <div className="">
            <img className="avatar" src={avatar} alt="avatar" height="50px" width="50px" />
          </div>
          <div className="">
            <h3 className="">{account}</h3>
          </div>
        </div>
      </div>
      <table className="table table-striped table-light">
        <thead>
          <tr>
            <th>Location</th>
            <th>Country</th>
            <th>Currency</th>
            <th>Sector</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td className="text-center">{location}</td>
            <td className="text-center">{country}</td>
            <td className="text-center">{currency}</td>
            <td className="text-center">{sector}</td>
          </tr>
        </tbody>
      </table>
    </div>
  )
}

OrderDetail.propTypes = propTypes
OrderDetail.defaultProps = defaultProps

export default OrderDetail
