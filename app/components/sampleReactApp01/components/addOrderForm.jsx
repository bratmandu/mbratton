import React, { Component } from 'react'
import { string, func } from 'prop-types'
import Oops from './oops'
import Button from './button'

class AddOrderForm extends Component {
  static propTypes = {
    addOrder: func.isRequired,
    id: string.isRequired
  }

  state = {
    orderDetails: {},
    errorsList: {},
    isValid: true
  }

  handleSubmit = () => {
    const { orderDetails, isValid } = this.state
    const { addOrder, id } = this.props

    if (isValid) {
      const parsedDetails = {
        id,
        account: orderDetails.account,
        contract: orderDetails.contract,
        price: parseInt(orderDetails.price, 10),
        quantity: parseInt(orderDetails.quantity, 10)
      }
      addOrder(parsedDetails)
    } else {
      console.log('not valid, do not submit!')
    }
  }

  validateForm = (name, value, min, max, type) => {
    const { errorsList, orderDetails } = this.state
    const orderDetailsUpdate = orderDetails
    const lettersReg = /^[a-z]+$/i
    const hasError = (min && max && (value < parseInt(min, 10) || value > parseInt(max, 10))) || (type === 'text' && !lettersReg.test(value))
    const updatedErrorsObject = errorsList

    updatedErrorsObject[name] = hasError

    const errorsArray = Object.values(updatedErrorsObject)

    orderDetailsUpdate[name] = value

    this.setState({
      errorsList: updatedErrorsObject,
      orderDetails: orderDetailsUpdate
    })

    return !errorsArray.some(x => x === true)
  }

  handleChange = ({
    target: {
      value, name, max, min, type
    }
  }) => {
    const isValid = this.validateForm(name, value, min, max, type)
    this.setState({
      isValid
    })
  }

  render() {
    const { orderDetails, isValid, errorsList } = this.state
    const { id } = this.props
    const btnDisabled = Object.keys(orderDetails).length < 4 || !isValid

    return (
      <div>
        {!isValid && <Oops />}
        <ul className="p-0 m-0">
          <li>
            <label htmlFor="account">
              <div className="col">Account</div>
              <input
                id={id}
                name="account"
                type="text"
                placeholder="Enter an account..."
                className={`${errorsList.account && 'has-error'} ml-3 col`}
                value={orderDetails.account ? orderDetails.account : ''}
                onChange={this.handleChange}
              />
            </label>
            <div className="" />
            <div className="error">{errorsList.account && 'Please enter a string.'}</div>
          </li>
          <li>
            <label htmlFor="contract">
              <div className="col">Contract</div>
              <input
                id="contract"
                name="contract"
                type="text"
                placeholder="Enter a contract..."
                className={`${errorsList.contract && 'has-error'} ml-3 col`}
                value={orderDetails.contract ? orderDetails.contract : ''}
                onChange={this.handleChange}
              />
            </label>
            <div className="" />
            <div className="error">{errorsList.contract && 'Please enter a string.'}</div>
          </li>
          <li>
            <label htmlFor="price">
              <div className="col">Price</div>
              <input
                id="price"
                name="price"
                type="number"
                placeholder="Max: 100"
                className={`${errorsList.price && 'has-error'} ml-3 col`}
                value={orderDetails.price ? orderDetails.price : ''}
                onChange={this.handleChange}
              />
            </label>
            <div className="" />
            <div className="error">{errorsList.price && 'Valid values: 1-100.'}</div>
          </li>
          <li>
            <label htmlFor="quantity">
              <div className="col">Quantity</div>
              <input
                id="quantity"
                name="quantity"
                type="number"
                placeholder="Max: 25"
                className={`${errorsList.quantity && 'has-error'} ml-3 col`}
                value={orderDetails.quantity ? orderDetails.quantity : ''}
                onChange={this.handleChange}
              />
            </label>
            <div className="" />
            <div className="error">{errorsList.quantity && 'Valid values: 1-25.'}</div>
          </li>
          <li>
            <Button className="btn btn-primary m-3" disabled={btnDisabled} onClick={this.handleSubmit}>
              Submit
            </Button>
          </li>
        </ul>
      </div>
    )
  }
}

export default AddOrderForm
