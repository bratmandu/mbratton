import React from 'react'
import { oneOfType, string, number } from 'prop-types'

const propTypes = {
  type: oneOfType([string, number]).isRequired
}

function Input(props) {
  return (
    <input {...props} />
  )
}

Input.propTypes = propTypes

export default Input
