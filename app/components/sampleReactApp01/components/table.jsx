import React, { Component } from 'react'
import {
  arrayOf, object, shape, string, number, func
} from 'prop-types'
import TableRow from './tableRow'

const propTypes = {
  data: arrayOf(object),
  sorting: shape({
    heading: string,
    order: string
  }),
  totalQuantity: number,
  handleSort: func,
  totalPrice: number,
  updateTotal: func,
  handleShowModal: func,
  handleDelete: func
}

const defaultProps = {
  data: [],
  sorting: {},
  totalQuantity: 0,
  totalPrice: 0,
  handleSort: () => {},
  updateTotal: () => {},
  handleShowModal: () => {},
  handleDelete: () => {}
}

class Table extends Component {
  updateTotal = (dir, key, id) => {
    const { updateTotal } = this.props
    updateTotal(dir, key, id)
  }

  render() {
    const {
      data,
      handleSort,
      sorting,
      totalQuantity,
      totalPrice,
      handleShowModal,
      handleDelete
    } = this.props

    return (
      <div className="table-responsive">
        <table className="table table-striped table-bordered">
          <thead>
            <tr className="text-center table-info">
              <th>Account</th>
              <th>Contract</th>
              <th className="cursor-pointer" onClick={() => handleSort('price')}>
                <i className={sorting.heading === 'price' ? `fa fa-sort-${sorting.order === 'asc' ? 'asc' : 'desc'} button-icon pr-2` : 'fa fa-sort button-icon pr-2'} />
                Price
              </th>
              <th className="cursor-pointer" onClick={() => handleSort('quantity')}>
                <i className={sorting.heading === 'quantity' ? `fa fa-sort-${sorting.order === 'asc' ? 'asc' : 'desc'} button-icon pr-2` : 'fa fa-sort button-icon pr-2'} />
                Quantity
              </th>
              <th>Delete</th>
            </tr>
          </thead>
          <tbody>
            { data.length
              ? data.map(row => (
                <TableRow
                  key={row.id}
                  {...row}
                  id={row.id.toString()}
                  updateTotal={this.updateTotal}
                  handleShowModal={handleShowModal}
                  handleDelete={handleDelete}
                />
              ))
              : (
                <tr>
                  <td colSpan="5">Sorry, there are currently no results available</td>
                </tr>
              )
            }
            { data.length
              ? (
                <tr>
                  <td />
                  <td className="text-center"><b>Total</b></td>
                  <td className="text-center">{totalPrice}</td>
                  <td className="text-center">{totalQuantity}</td>
                  <td />
                </tr>
              )
              : null
            }
          </tbody>
        </table>
      </div>
    )
  }
}

Table.propTypes = propTypes
Table.defaultProps = defaultProps

export default Table
