import React from 'react'
import { string } from 'prop-types'

const propTypes = {
  type: string,
  children: string
}

const defaultProps = {
  type: '',
  children: ''
}

function getAlertIcon(type) {
  switch (type) {
    case 'danger':
      return 'ban'
    case 'success':
      return 'check'
    default:
      return ''
  }
}

function Alert({ type, children }) {
  return (
    <div className={`alert alert-${type} d-flex flex-row p-4`} role="alert">
      <div className="alert-icon">
        <i className={`fa fa-${getAlertIcon(type.toLowerCase())} icon-medium`} aria-hidden="true" />
      </div>
      <div className="alert-message">
        {children}
      </div>
    </div>
  )
}

Alert.propTypes = propTypes
Alert.defaultProps = defaultProps

export default Alert
