import React from 'react'
import {
  oneOfType, string, node, object, func
} from 'prop-types'

const propTypes = {
  children: oneOfType([
    string,
    object,
    node
  ]),
  onClick: func
}

const defaultProps = {
  children: '',
  onClick: null
}

function Button({ children, onClick, ...rest }) {
  return (
    <button
      {...rest}
      onClick={onClick}
      type="button"
    >
      {children}
    </button>
  )
}

Button.propTypes = propTypes
Button.defaultProps = defaultProps

export default Button
