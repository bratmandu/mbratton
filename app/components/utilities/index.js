function copyToClipboard(e) {
  const elem = document.getElementById(e.target.getAttribute('element'))
  if (elem === null || elem === undefined) return
  const range = document.createRange()
  range.selectNode(elem)
  window.getSelection().removeAllRanges()
  window.getSelection().addRange(range)
  document.execCommand('copy')
  window.getSelection().removeAllRanges()
}

function incrementValue(currentValue) {
  return currentValue + 1
}

function decrementValue(currentValue) {
  return currentValue - 1
}

// export default {
//   copyToClipboard
// }

export {
  incrementValue,
  decrementValue,
  copyToClipboard
}
